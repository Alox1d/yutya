package ru.alox1d.yutya

import android.app.Application
import ru.alox1d.yutya.data.MaskDatabase
import ru.alox1d.yutya.di.AppComponent
import ru.alox1d.yutya.di.DaggerAppComponent

class App : Application() {
    val appComponent: AppComponent = DaggerAppComponent.create()
    private var mAppExecutors: AppExecutors? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        mAppExecutors = AppExecutors()
    }
    val database: MaskDatabase?
        get() = MaskDatabase.getInstance()

    val appExecutors: AppExecutors?
        get() = mAppExecutors

    companion object {
        var instance: App? = null
            private set
    }
}
