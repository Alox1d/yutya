package ru.alox1d.yutya.domain.validator

import ru.alox1d.yutya.domain.FieldError

object IntValidator : Validator<String?, Int, FieldError>() {
    override fun checkRaw(item: String?) = if (item.isNullOrEmpty()) FieldError.EMPTY_FIELD else null

    override fun checkTransformed(item: Int) = if (item == 0) FieldError.ZERO_FIELD else null

    override fun transform(item: String?) = item?.toInt() ?: 0
}
