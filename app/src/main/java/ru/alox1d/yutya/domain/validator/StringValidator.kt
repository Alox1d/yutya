package ru.alox1d.yutya.domain.validator

import ru.alox1d.yutya.domain.FieldError

object StringValidator : Validator<String?, String, FieldError>() {
    override fun checkRaw(item: String?) = if (item.isNullOrEmpty()) FieldError.EMPTY_FIELD else null

    override fun checkTransformed(item: String) = if (item == "") FieldError.ZERO_FIELD else null

    override fun transform(item: String?) = item ?: ""
}
