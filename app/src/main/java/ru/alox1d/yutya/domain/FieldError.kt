package ru.alox1d.yutya.domain

enum class FieldError { EMPTY_FIELD, ZERO_FIELD }
