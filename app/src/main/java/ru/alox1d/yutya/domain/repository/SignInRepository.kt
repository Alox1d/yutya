package ru.alox1d.yutya.domain.repository

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class SignInRepository(private val bgDispatcher: CoroutineDispatcher):ISignInRepository {
    override suspend operator fun invoke(params: ISignInRepository.Params): Boolean = withContext(bgDispatcher) {
        params.login=="alox1d@mail.ru"&&params.pass=="Qwerty!@#1"
        }


}
