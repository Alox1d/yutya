package ru.alox1d.yutya.domain.repository

import android.util.Log
import androidx.lifecycle.LiveData
import ru.alox1d.yutya.data.Mask
import ru.alox1d.yutya.data.MaskDatabase

class MaskRepository {
    // mask id
    private var id: Int = -1
    // database
    val database: MaskDatabase = MaskDatabase.getInstance()!!

    // LiveData: all masks
    fun getAllByLiveData(): LiveData<List<Mask>>? {
        lgd("getAllByLiveData()")
        return database.maskDao()!!.getAllByLiveData()
    }

    companion object {
        // logcat
        val tag = "MYLOG "+"MaskRepository"
        fun lgi(s: String) { Log.i(tag, s)}
        fun lgd(s: String) { Log.d(tag, s)}
        fun lge(s: String) { Log.e(tag, s)}

        fun getInstance(): MaskRepository {
            lgd ("getInstance()")
            return MaskRepository()
        }
    }
}