package ru.alox1d.yutya.domain.repository

interface ISignInRepository {
    suspend operator fun invoke(params: Params):Boolean // async - остснавливаемая (асигхронная) функция
    data class Params(
        val login: String?,
        val pass: String?
    )
}