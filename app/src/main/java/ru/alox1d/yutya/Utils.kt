package ru.alox1d.yutya

import android.content.Context
import android.graphics.BitmapFactory

import android.graphics.Bitmap

import android.content.res.AssetManager
import java.io.IOException
import java.io.InputStream


class Utils {
    companion object{
    // Custom method to get assets folder image as bitmap
     fun getBitmapFromAssets(context: Context, fileName: String): Bitmap? {
        /*
            AssetManager
                Provides access to an application's raw asset files.
        */

        /*
            public final AssetManager getAssets ()
                Retrieve underlying AssetManager storage for these resources.
        */
        val am: AssetManager = context.assets
        var `is`: InputStream? = null
        try {
            /*
                public final InputStream open (String fileName)
                    Open an asset using ACCESS_STREAMING mode. This provides access to files that
                    have been bundled with an application as assets -- that is,
                    files placed in to the "assets" directory.

                    Parameters
                        fileName : The name of the asset to open. This name can be hierarchical.
                    Throws
                        IOException
            */
            `is` = am.open(fileName)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        /*
            BitmapFactory
                Creates Bitmap objects from various sources, including files, streams, and byte-arrays.
        */

        /*
            public static Bitmap decodeStream (InputStream is)
                Decode an input stream into a bitmap. If the input stream is null, or cannot
                be used to decode a bitmap, the function returns null. The stream's
                position will be where ever it was after the encoded data was read.

                Parameters
                    is : The input stream that holds the raw data to be decoded into a bitmap.
                Returns
                    The decoded bitmap, or null if the image data could not be decoded.
        */
        return BitmapFactory.decodeStream(`is`)
    }}
}