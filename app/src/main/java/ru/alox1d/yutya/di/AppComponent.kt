package ru.alox1d.yutya.di

import dagger.Component
import ru.alox1d.yutya.di.module.DomainModule
import ru.alox1d.yutya.di.module.PresentationModule
import ru.alox1d.yutya.presentation.loginform.LoginFragment

@Component(modules = [DomainModule::class, PresentationModule::class])
interface AppComponent {
    fun inject(fragment: LoginFragment)
}
