package ru.alox1d.yutya.di.module

import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import ru.alox1d.yutya.domain.repository.SignInRepository

@Module
class DomainModule {
    @Provides
    fun provideBackgroundDispatcher(): CoroutineDispatcher = Dispatchers.Default

    @Provides
    fun provideSignInRepo(
        bgDispatcher: CoroutineDispatcher
    ) = SignInRepository(bgDispatcher)
}
