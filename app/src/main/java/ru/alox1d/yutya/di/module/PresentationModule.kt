package ru.alox1d.yutya.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.alox1d.yutya.di.annotation.Login
import ru.alox1d.yutya.domain.repository.SignInRepository
import ru.alox1d.yutya.presentation.loginform.LoginViewModelFactory

@Module
class PresentationModule {
    @Provides
    @Login
    fun provideInvestFormViewModelFactory(
        signInRepository: SignInRepository
    ): ViewModelProvider.Factory = LoginViewModelFactory(signInRepository)
}
