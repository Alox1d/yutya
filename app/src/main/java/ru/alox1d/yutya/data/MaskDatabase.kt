package ru.alox1d.yutya.data

import android.content.Context
import android.util.Log
import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import ru.alox1d.yutya.App
import ru.alox1d.yutya.AppExecutors

@Database(entities = [Mask::class], version = 3)
 abstract class MaskDatabase : RoomDatabase() {
    abstract fun maskDao(): MaskDao?
    // LiveData: check database created?
    private val mIsDbCreated = MutableLiveData<Boolean>()
    open fun getDbCreated(): LiveData<Boolean?>? = mIsDbCreated
    open fun setDbCreated() = mIsDbCreated.postValue(true)
    private fun updateDatabaseCreated(context: Context?) {
        if (context!!.getDatabasePath(DATABASE_NAME).exists()) {
            setDbCreated();
        }
    }
    companion object{
        @Volatile
        private var maskDb: MaskDatabase? = null

        @VisibleForTesting
        val DATABASE_NAME = "MASKS_DB"

        // Simulation time range: 1s = 1000ms
        val DELAY_MS: Long = 3000

        fun getInstance(): MaskDatabase? {
            lgd("getInstance()")
            lgd("userDb: ${maskDb.toString()}")

            if (maskDb == null) {
                lgd("create demo data")

                synchronized(MaskDatabase::class.java) {
                    if (maskDb == null) {

                        val mApp = App.instance
                        val mExecutors = mApp!!.appExecutors

                        maskDb = mExecutors?.let { buildDatabase(mApp, it) }
                        maskDb!!.updateDatabaseCreated(mApp)
                    }
                }
            }
            return maskDb
        }
        // logcat
        val tag = "MYLOG "+"UserDatabase"
        fun lgi(s: String) { Log.i(tag, s)}
        fun lgd(s: String) { Log.d(tag, s)}
        fun lge(s: String) { Log.e(tag, s)}
        private fun buildDatabase(
            appContext: Context,
            executors: AppExecutors
        ): MaskDatabase? {
            lgd("buildDatabase()")
            return Room.databaseBuilder<MaskDatabase> (
                appContext,
                MaskDatabase::class.java,
                DATABASE_NAME
// room callback
            ).addCallback(object : Callback() {
                // room callback
                override fun onCreate(
                    @NonNull db: SupportSQLiteDatabase
                ) {
                    super.onCreate(db)
                    executors.diskIO().execute {
                        // Simulate a long-running operation
//                        delaySimulation() // new database
                        val database: MaskDatabase? = getInstance() // Generate the data for pre-population
                        val users: List<Mask> =
                            DataGenerator.generateUsers()
                        insertData(database, users) // notify that the database was created
                        database?.setDbCreated()
                    }
                }
            })
                .build()
        }private fun insertData(
            database: MaskDatabase?,
            masks: List<Mask>) {
            database!!.runInTransaction {
                database.maskDao()!!.insertAll(masks)
            }
        }

        private fun delaySimulation() {
            try {
                Thread.sleep(DELAY_MS)
            } catch (ignored: InterruptedException) {
            }
        }
    }




}
