package ru.alox1d.yutya.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MaskDao {
    @Insert
    fun insert(user: Mask)

    @Insert
    fun insertAll(users: List<Mask>?)

    @Delete
    fun delete(user: Mask)

    @Delete
    fun deleteAll(users: List<Mask>?)

    @Update
    fun update(user: Mask)

    @Query("SELECT * FROM Mask")
    fun getAllByLiveData(): LiveData<List<Mask>>?

    @Query("SELECT * FROM Mask WHERE uid = :uid")
    fun getByUid(uid: Int): LiveData<Mask>?
}