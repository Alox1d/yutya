package ru.alox1d.yutya.data

object DataGenerator {

    private val PATHS = arrayOf(
        "masks/1_deer_face.png",
        "masks/2_shower_hat.png",
        "masks/3_deer_horns.png",
        "masks/4_carnival_mask.png",
        "masks/5_bear_nose_ears.png",
        "masks/6_irish_hat.png"
    )
    private val coordinates = arrayOf(
        arrayOf(0.0f, 0.06f, 0.08f),
        arrayOf(0.0f, 0.1f, 0.02f),
        arrayOf(0.0f, 0.2f, 0.02f),
        arrayOf(0.0f, 0.026f, 0.065f),
        arrayOf(0.0f, 0.06f, 0.08f),
        arrayOf(0.0f, 0.1f, 0.09f),
    )

    fun generateUsers(): List<Mask> {
        val userEntities: MutableList<Mask> = ArrayList()
        for (i in PATHS.indices) {
            val product = Mask()
            product.path = PATHS[i]
            product.x = coordinates[i][0]
            product.y = coordinates[i][1]
            product.z = coordinates[i][2]
            userEntities.add(product)
        }
        return userEntities
    }
}