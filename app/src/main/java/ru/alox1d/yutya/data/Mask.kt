package ru.alox1d.yutya.data

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
class Mask {
    @PrimaryKey(autoGenerate = true)
    var uid = 0
    var name: String? = null
    var path: String? = ""
    var x:Float = 0.0f
    var y:Float = 0.0f
    var z:Float = 0.0f

    override fun toString(): String {
        return "MaskTable {" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                '}'
    }
}