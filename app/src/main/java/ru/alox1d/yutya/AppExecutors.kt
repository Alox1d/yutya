package ru.alox1d.yutya

import android.os.Handler
import android.os.Looper
import androidx.annotation.NonNull
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class AppExecutors(
    diskIO: Executor,
    networkIO: Executor,
    mainThread: Executor
) {
    private val mDiskIO: Executor = diskIO
    private val mNetworkIO: Executor = networkIO
    private val mMainThread: Executor = mainThread

    fun diskIO() = mDiskIO
    fun networkIO() = mNetworkIO
    fun mainThread() = mMainThread

    constructor() : this(
        Executors.newSingleThreadExecutor(),
        Executors.newFixedThreadPool(3),
        MainThreadExecutor()
    ) {}

    private class MainThreadExecutor : Executor {
        private val mainThreadHandler: Handler =
            Handler(Looper.getMainLooper())

        override fun execute(@NonNull command: Runnable?) {
            mainThreadHandler.post(command!!)
        }
    }
}