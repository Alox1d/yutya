package ru.alox1d.yutya.presentation.loginform

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.alox1d.yutya.domain.FieldError
import ru.alox1d.yutya.domain.repository.ISignInRepository
import ru.alox1d.yutya.domain.repository.SignInRepository
import ru.alox1d.yutya.domain.validator.StringValidator
import ru.alox1d.yutya.presentation.Event

class LoginViewModel(private val signIn: SignInRepository) : ViewModel() {
    private val _signInResult = MutableLiveData<Boolean>()
    private val _isLoading = MutableLiveData<Boolean>()
    private val _loginError = MutableLiveData<FieldError>()
    private val _passError = MutableLiveData<FieldError>()
    private val _navigateToHomeEvent = MutableLiveData<Event<Unit>>()
    private val _navigateToRegistrationEvent = MutableLiveData<Event<Unit>>()

    val login = MutableLiveData<String>()
    val pass = MutableLiveData<String>()
    val signInResult: LiveData<Boolean> = _signInResult
    val isProfitsLoading: LiveData<Boolean> = _isLoading
    val loginError: LiveData<FieldError> = _loginError
    val passError: LiveData<FieldError> = _passError
    val navigateToResultsEvent: LiveData<Event<Unit>> = _navigateToHomeEvent
    val navigateToRegistrationEvent: LiveData<Event<Unit>> = _navigateToRegistrationEvent
    init {
        login.value = "alox1d@mail.ru"
        pass.value = "Qwerty!@#1"
    }
    fun signIn() {
        _loginError.value = StringValidator.check(login.value)
        _passError.value = StringValidator.check(pass.value)

        if (_loginError.value != null) return
        if (_passError.value != null) return

//        val login = StringValidator.transform(login.value)
        val params = ISignInRepository.Params(login.value, pass.value)

        viewModelScope.launch {
            _isLoading.value = true
            _signInResult.value = signIn(params)
            _isLoading.value = false
            if (_signInResult.value == true)
                _navigateToHomeEvent.value = Event(Unit)
        }

    }
    fun signUp(){
        _navigateToRegistrationEvent.value = Event(Unit)

    }
}
