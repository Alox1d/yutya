package ru.alox1d.yutya.presentation.loginform

import android.app.ActivityManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.ar.core.ArCoreApk
import ru.alox1d.yutya.App
import ru.alox1d.yutya.R
import ru.alox1d.yutya.databinding.FragmentLoginFormBinding
import ru.alox1d.yutya.di.annotation.Login
import ru.alox1d.yutya.domain.FieldError
import ru.alox1d.yutya.presentation.MIN_OPENGL_VERSION
import ru.alox1d.yutya.presentation.closeKeyboard
import javax.inject.Inject

class LoginFragment : Fragment() {
    private val loginFormViewModel by activityViewModels<LoginViewModel> { loginFormViewModelFactory }
    private lateinit var binding: FragmentLoginFormBinding

    @Inject
    @Login
    lateinit var loginFormViewModelFactory: ViewModelProvider.Factory

    private val loginErrorObserver = { error: FieldError? ->
        binding.balanceLayout.error = when (error) {
            FieldError.EMPTY_FIELD -> getString(R.string.login_form_error_empty_login)
            FieldError.ZERO_FIELD -> getString(R.string.login_form_error_empty_login)
            else -> null
        }
    }
    private val resultSignInObserver = { success: Boolean ->
        if(!success)
            Toast.makeText(requireContext(), "Неверный логин/пароль", Toast.LENGTH_SHORT).show()
    }


    private val passErrorObserver = { error: FieldError? ->
        binding.percentLayout.error = when (error) {
            FieldError.EMPTY_FIELD -> getString(R.string.login_form_error_empty_pass)
            FieldError.ZERO_FIELD -> getString(R.string.login_form_error_empty_pass)
            else -> null
        }
    }



    override fun onAttach(context: Context) {
        super.onAttach(context)

        (activity?.applicationContext as App).appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoginFormBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.loginFormViewModel = loginFormViewModel

        loginFormViewModel.navigateToResultsEvent.observe(viewLifecycleOwner, { event ->
            event.getDataIfNotHandled()?.let {
                closeKeyboard()
                navigateToHome()
            }
        })
        loginFormViewModel.navigateToRegistrationEvent.observe(viewLifecycleOwner, { event ->
            event.getDataIfNotHandled()?.let {
                Toast.makeText(requireContext(), getString(R.string.in_development), Toast.LENGTH_SHORT).show()
            }
        })

        loginFormViewModel.loginError.observe(viewLifecycleOwner, loginErrorObserver)
        loginFormViewModel.passError.observe(viewLifecycleOwner, passErrorObserver)
        loginFormViewModel.signInResult.observe(viewLifecycleOwner, resultSignInObserver)

        return binding.root
    }

    private fun navigateToHome() {
//        if (checkIsSupportedDeviceOrFinish())
        findNavController().navigate(R.id.action_login_form_fragment_to_home_fragment)
//        else
//            Toast.makeText(requireContext(), getString(R.string.unsupp_device), Toast.LENGTH_SHORT).show()
    }
    /*
    Method that checks if AR Core is available on this device.
    */
    private fun checkIsSupportedDeviceOrFinish(): Boolean {
        if (ArCoreApk.getInstance().checkAvailability(activity) === ArCoreApk.Availability.UNSUPPORTED_DEVICE_NOT_CAPABLE) {
            return false
        }
        val openGlVersionString = (activity?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
            .deviceConfigurationInfo
            .glEsVersion
        if (java.lang.Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {

            return false
        }
        return true
    }
}
