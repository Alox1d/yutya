package ru.alox1d.yutya.presentation.chart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.github.mikephil.charting.charts.LineChart
import ru.alox1d.yutya.databinding.FragmentMaskOptionsBinding
import ru.alox1d.yutya.presentation.ar.SearchCompanionViewModel

class chartFragment : Fragment() {
    private val searchCompanionViewModel by activityViewModels<SearchCompanionViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentMaskOptionsBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.searchCompanionViewModel = searchCompanionViewModel

        return binding.root
    }

    private fun setupChart(chart: LineChart) {
        chart.description.text = ""
    }
}
