package ru.alox1d.yutya.presentation.home

import android.app.ActivityManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.github.mikephil.charting.charts.LineChart
import com.google.ar.core.ArCoreApk
import ru.alox1d.yutya.R
import ru.alox1d.yutya.databinding.FragmentHomeBinding
import ru.alox1d.yutya.presentation.MIN_OPENGL_VERSION
import ru.alox1d.yutya.presentation.ar.SearchCompanionViewModel
import ru.alox1d.yutya.presentation.closeKeyboard
import ru.alox1d.yutya.presentation.loginform.LoginViewModel

class HomeFragment : Fragment() {
    private val loginViewModel by activityViewModels<LoginViewModel>()
    private val searchCompanionViewModel by activityViewModels<SearchCompanionViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentHomeBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.loginViewModel = loginViewModel
        binding.searchCompanionViewModel = searchCompanionViewModel

        searchCompanionViewModel.searchEvent.observe(viewLifecycleOwner, { event ->
            event.getDataIfNotHandled()?.let {
                navigateToSearch()
            }
        })
        searchCompanionViewModel.setupEvent.observe(viewLifecycleOwner, { event ->
            event.getDataIfNotHandled()?.let {
                navigateToSetupMasks()
            }
        })


        return binding.root
    }
    private fun navigateToSearch() {
        if (checkIsSupportedDeviceOrFinish())
        findNavController().navigate(R.id.action_homeFragment_to_search_companion_fragment)
        else
            Toast.makeText(requireContext(), getString(R.string.unsupp_device), Toast.LENGTH_SHORT).show()
    }
    private fun navigateToSetupMasks() {
        if (checkIsSupportedDeviceOrFinish())
            findNavController().navigate(R.id.action_home_fragment_to_mask_options_fragment)
        else
            Toast.makeText(requireContext(), getString(R.string.unsupp_device), Toast.LENGTH_SHORT).show()
    }
    /*
    Method that checks if AR Core is available on this device.
    */
    private fun checkIsSupportedDeviceOrFinish(): Boolean {
        if (ArCoreApk.getInstance().checkAvailability(activity) === ArCoreApk.Availability.UNSUPPORTED_DEVICE_NOT_CAPABLE) {
            return false
        }
        val openGlVersionString = (activity?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
            .deviceConfigurationInfo
            .glEsVersion
        if (java.lang.Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {

            return false
        }
        return true
    }
}
