package ru.alox1d.yutya.presentation.loginform

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.alox1d.yutya.domain.repository.SignInRepository

class LoginViewModelFactory(
    private val signIn: SignInRepository
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) = LoginViewModel(signIn) as T
}
