package ru.alox1d.yutya.presentation.ar

import android.app.ActivityManager
import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.google.ar.core.*
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.*
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.AugmentedFaceNode
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.alox1d.yutya.R
import ru.alox1d.yutya.Utils
import ru.alox1d.yutya.data.Mask
import ru.alox1d.yutya.domain.repository.MaskRepository
import java.util.*
import androidx.appcompat.app.AppCompatActivity






class CustomARFragment : ArFragment() {
    private val MIN_OPENGL_VERSION = 3.0
    private val faceNodeMap = HashMap<AugmentedFace, AugmentedFaceNode>()
    private  var bitmap:Bitmap? = null
    private lateinit var mask: Mask
    private  var snackbar: Snackbar?=null
    override fun onAttach(context: Context) {
        if (!checkIsSupportedDeviceOrFinish())
    requireActivity().onBackPressed()
        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()

        //TODO snackbar not showing sometimes
//        snackbar = Snackbar.make(activity!!.window.decorView.rootView, getString(R.string.searching_companionn), Snackbar.LENGTH_INDEFINITE)
//        snackbar?.setAction(getString(R.string.cancel), View.OnClickListener {
//            activity?.onBackPressed()
//        })
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Toast.makeText(activity, getString(R.string.searching_companionn), Toast.LENGTH_LONG).show()

        requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        super.onViewCreated(view, savedInstanceState)

    }
    override fun onDetach() {
        super.onDetach()
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
//
//        snackbar?.dismiss()
//        snackbar = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val fragmentView = super.onCreateView(inflater, container, savedInstanceState)
        //hide some of the plane finders and discovery since its not needed for augmented images.

        planeDiscoveryController.hide()
        planeDiscoveryController.setInstructionView(null)



        //I'm so sorry for this - издержки хакатона!
        val masks = MaskRepository.getInstance()
        masks.getAllByLiveData()?.observe(
            viewLifecycleOwner,{
                if (it.size>0){
                    val randomNumber = kotlin.random.Random.Default.nextInt(0,it.size)
                    mask = it[randomNumber]
                    val imgPath = mask.path ?: ""
                    bitmap =
                        Utils.getBitmapFromAssets(
                            requireContext(),
                            imgPath
                        )
                }

            }
        )
        return fragmentView
    }
    override fun getSessionConfiguration(session: Session): Config {
        val config = Config(session)
        config.augmentedFaceMode = Config.AugmentedFaceMode.MESH3D
        return config
    }

    /**
     * Tell ARCore that we need to use the front camera.
     */
    override fun getSessionFeatures(): Set<Session.Feature> {
        snackbar?.show()

        return EnumSet.of<Session.Feature>(Session.Feature.FRONT_CAMERA)
    }





    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val sceneView = arSceneView
// This is important to make sure that the camera stream renders first so that
        // the face mesh occlusion works correctly.
        arSceneView.setCameraStreamRenderPriority(Renderable.RENDER_PRIORITY_FIRST)
        val scene = sceneView?.scene
        // Check for face detections
        scene?.addOnUpdateListener {
            val faceList = sceneView?.session!!.getAllTrackables(AugmentedFace::class.java)
            // Make new AugmentedFaceNodes for any new faces.
            for (face in faceList) {
                if (!faceNodeMap.containsKey(face)) {
                    val faceNode = AugmentedFaceNode(face)
                    faceNode.setParent(scene)

                    val imageView = activity?.findViewById<ImageView>(R.id.imageCard)
                    imageView?.setImageBitmap(bitmap)
                    val layout = R.layout.image_ar_mask
                    ViewRenderable.builder().setView(activity, layout).build()
                        .thenAccept {

                            val lightBulb = Node()
                            val localPosition = Vector3()
                            //lift the light bulb to be just above your head.
                            localPosition.set(mask.x, mask.y, mask.z)
                            lightBulb.localPosition = localPosition
                            lightBulb.setParent(faceNode)
                            it.verticalAlignment = ViewRenderable.VerticalAlignment.CENTER
                            it.horizontalAlignment = ViewRenderable.HorizontalAlignment.CENTER
                            val v = it.view as ImageView
                            v.setImageBitmap(bitmap)
                            lightBulb.renderable = it

                        }

                    faceNodeMap[face] = faceNode
                }
            }

            // Remove any AugmentedFaceNodes associated with an AugmentedFace that stopped tracking.
            val faceIterator = faceNodeMap.entries.iterator()
            while (faceIterator.hasNext()) {
                val entry = faceIterator.next()
                val face = entry.key
                if (face.trackingState == TrackingState.STOPPED) {
                    val faceNode = entry.value
                    faceNode.setParent(null)
                    faceNode.children.clear()
                    faceIterator.remove()
                }
            }

        }

    }



    /*
      Method that checks if AR Core is available on this device.
   */
    private fun checkIsSupportedDeviceOrFinish(): Boolean {
        if (ArCoreApk.getInstance().checkAvailability(activity) === ArCoreApk.Availability.UNSUPPORTED_DEVICE_NOT_CAPABLE) {
            return false
        }
        val openGlVersionString = (activity?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
            .deviceConfigurationInfo
            .glEsVersion
        if (java.lang.Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {

            return false
        }
        return true
    }
}

//
//import android.net.Uri
//import android.os.Bundle
//import android.view.View
//import android.widget.ImageView
//import android.widget.Toast
//import com.google.ar.core.*
//import com.google.ar.sceneform.ArSceneView
//import com.google.ar.sceneform.Node
//import com.google.ar.sceneform.collision.Box
//import com.google.ar.sceneform.math.Vector3
//import com.google.ar.sceneform.rendering.*
//import com.google.ar.sceneform.ux.ArFragment
//import com.google.ar.sceneform.ux.ArFrontFacingFragment
//import com.google.ar.sceneform.ux.AugmentedFaceNode
//import com.gorisse.thomas.sceneform.light.LightEstimationConfig
//import com.gorisse.thomas.sceneform.lightEstimationConfig
//import java.util.HashMap
//import java.util.HashSet
//import java.util.concurrent.CompletableFuture
//import ru.alox1d.yutya.R
//
//class CustomARFragment : ArFragment() {
//    private val loaders: MutableSet<CompletableFuture<*>> = HashSet()
//    private var arFragment: ArFrontFacingFragment? = null
//    private var sceneView: ArSceneView? = null
//    private val faceTexture: Texture? = null
//    private var faceModel: ModelRenderable? = null
//    private var image: ViewRenderable? = null
//    private val facesNodes = HashMap<AugmentedFace, AugmentedFaceNode>()
//    private val faceNodeMap = HashMap<AugmentedFace, AugmentedFaceNode>()
//    override fun onCreateSessionConfig(session: Session): Config {
//        val filter = CameraConfigFilter(session)
//        filter.facingDirection = CameraConfig.FacingDirection.FRONT
//        session.cameraConfig = session.getSupportedCameraConfigs(filter)[0]
//        val config = super.onCreateSessionConfig(session)
////        config.planeFindingMode = Config.PlaneFindingMode.DISABLED
////        config.lightEstimationMode = Config.LightEstimationMode.DISABLED
//        return config
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        instructionsController.isEnabled = false
//
//        // Disable the light estimation mode because it's not compatible with the front face camera
//        arSceneView.lightEstimationConfig = LightEstimationConfig.DISABLED
//
//        // Hide plane indicating dots
//        arSceneView.planeRenderer.isVisible = false
//        // Disable the rendering of detected planes.
//        arSceneView.planeRenderer.isEnabled = false
//    }
//
//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//
//        this.sceneView = arSceneView
//// This is important to make sure that the camera stream renders first so that
//        // the face mesh occlusion works correctly.
//        arSceneView.setCameraStreamRenderPriority(Renderable.RENDER_PRIORITY_FIRST)
//        val scene = sceneView?.scene
//        // Check for face detections
//        scene?.addOnUpdateListener {
//            val faceList = sceneView?.session!!.getAllTrackables(AugmentedFace::class.java)
//            // Make new AugmentedFaceNodes for any new faces.
//            for (face in faceList) {
//                if (!faceNodeMap.containsKey(face)) {
//                    val faceNode = AugmentedFaceNode(face)
//                    faceNode.setParent(scene)
//
//                    //add light bulb above head
//                    ViewRenderable.builder().setView(activity, R.layout.img).build()
//                        .thenAccept {
//                            val lightBulb = Node()
//                            val localPosition = Vector3()
//                            //lift the light bulb to be just above your head.
//                            localPosition.set(0.0f, 0.17f, 0.0f)
//                            lightBulb.localPosition = localPosition
//                            lightBulb.setParent(faceNode)
//                            lightBulb.renderable = it
//
//                        }
//
//                    //give the face a little blush
//                    Texture.builder()
//                        .setSource(activity, R.drawable.blush_texture)
//                        .build()
//                        .thenAccept { texture ->
//                            faceNode.faceMeshTexture = texture
//                        }
//                    faceNodeMap[face] = faceNode
//                }
//            }
//
//            // Remove any AugmentedFaceNodes associated with an AugmentedFace that stopped tracking.
//            val faceIterator = faceNodeMap.entries.iterator()
//            while (faceIterator.hasNext()) {
//                val entry = faceIterator.next()
//                val face = entry.key
//                if (face.trackingState == TrackingState.STOPPED) {
//                    val faceNode = entry.value
//                    faceNode.setParent(null)
//                    faceNode.children.clear()
//                    faceIterator.remove()
//                }
//            }
//        }
//
//
////        setOnAugmentedFaceUpdateListener { augmentedFace: AugmentedFace ->
////            onAugmentedFaceTrackingUpdate(
////                augmentedFace
////            )
////        }
////        loadModels();
////        loadTextures()
//
////        arSceneView.scene.addOnUpdateListener {
////            val faceList = sceneView!!.session!!.getAllTrackables(AugmentedFace::class.java)
////            // Make new AugmentedFaceNodes for any new faces.
////            for (face in faceList) {
////                if (!faceNodeMap.containsKey(face)) {
////                    val faceNode = AugmentedFaceNode(face)
////                    faceNode.setParent(scene)
////
////                    //add light bulb above head
////                    ViewRenderable.builder().setView(activity, R.layout.img).build()
////                        .thenAccept {
////                            val lightBulb = Node()
////                            lightBulb.setParent(faceNode)
////                            lightBulb.renderable = it
////
////                        }
////
////                    //give the face a little blush
//////                    Texture.builder()
//////                        .setSource(this, R.drawable.blush_texture)
//////                        .build()
//////                        .thenAccept { texture ->
//////                            faceNode.faceMeshTexture = texture
//////                        }
////                    faceNodeMap[face] = faceNode
////                }
////            }
//////        arFragment!!.setOnAugmentedFaceUpdateListener { augmentedFace: AugmentedFace ->
//////            onAugmentedFaceTrackingUpdate(
//////                augmentedFace
//////            )
//////        }
////            // Remove any AugmentedFaceNodes associated with an AugmentedFace that stopped tracking.
////            val faceIterator = faceNodeMap.entries.iterator()
////            while (faceIterator.hasNext()) {
////                val entry = faceIterator.next()
////                val face = entry.key
////                if (face.trackingState == TrackingState.STOPPED) {
////                    val faceNode = entry.value
////                    faceNode.setParent(null)
////                    faceNode.children.clear()
////                    faceIterator.remove()
////                }
////            }
////        }
//    }
////    override fun onDestroy() {
////        super.onDestroy()
////        for (loader in loaders) {
////            if (!loader.isDone) {
////                loader.cancel(true)
////            }
////        }
////    }
//
//    fun onAugmentedFaceTrackingUpdate(augmentedFace: AugmentedFace) {
//        if (faceModel == null || image == null) {
//            return
//        }
//        val existingFaceNode = facesNodes[augmentedFace]
//        when (augmentedFace.trackingState) {
//            TrackingState.TRACKING -> if (existingFaceNode == null) {
//                val faceNode = AugmentedFaceNode(augmentedFace)
////                faceNode.parent = sceneView!!.scene
//                ViewRenderable.builder()
//                    .setView(
//                        activity,
//                        R.layout.img
//                    )
//                    //                    .setVerticalAlignment(ViewRenderable.VerticalAlignment.CENTER)
//                    .build()
//                    .thenAccept { renderable: ViewRenderable? ->
////                        val node = Node()
////                        node.parent = faceNode
////                        node.renderable = renderable
//                        val box = faceNode.collisionShape as Box
//                        val imgView = renderable?.view as ImageView
//                        imgView.layoutParams.height = box.size.x.toInt()/2
//                        imgView.layoutParams.height = box.size.y.toInt()/2
//                        image = renderable
//
//                    }
//
////                val modelInstance = faceNode.setFaceRegionsRenderable(faceModel);
////                modelInstance.setShadowCaster(false);
////                modelInstance.setShadowReceiver(true);
//
////                    image.setVerticalAlignment(ViewRenderable.VerticalAlignment.CENTER);
//                faceNode.renderable = image
//
//                arSceneView.getScene().addChild(faceNode);
//                facesNodes[augmentedFace] = faceNode
//            }
//            TrackingState.STOPPED -> {
//                if (existingFaceNode != null) {
//                    existingFaceNode.setRenderable(null)
//                    sceneView!!.scene.removeChild(existingFaceNode)
//                }
//                facesNodes.remove(augmentedFace)
//            }
//        }}
//    private fun loadTextures() {
////        loaders.add(Texture.builder()
////                .setSource(this, Uri.parse("masks/freckles.png"))
////                .setUsage(Texture.Usage.COLOR_MAP)
////                .build()
////                .thenAccept(texture -> faceTexture = texture)
////                .exceptionally(throwable -> {
////                    Toast.makeText(this, "Unable to load texture", Toast.LENGTH_LONG).show();
////                    return null;
////                }));
//        loaders.add(ViewRenderable.builder()
//            .setView(
//                activity,
//                R.layout.img
//            )
//            .setVerticalAlignment(ViewRenderable.VerticalAlignment.CENTER)
//            .setHorizontalAlignment(ViewRenderable.HorizontalAlignment.CENTER)
//            .build()
//            .thenAccept { renderable: ViewRenderable ->
//                val imgView = renderable.view as ImageView
//                image = renderable
//            })
//    }
//    private fun loadModels() {
//        loaders.add(ModelRenderable.builder()
//            .setSource(activity, Uri.parse("models/fox.glb"))
//
//            .setIsFilamentGltf(true)
//            .build()
//            .thenAccept { model: ModelRenderable? ->
////                val size = Vector3(100f,0.001f,100f)
////                val center = Vector3(0f,0f,0f)
////                model?.collisionShape = Box(size, center)
//                faceModel = model }
//            .exceptionally { throwable: Throwable? ->
//                Toast.makeText(activity, "Unable to load renderable", Toast.LENGTH_LONG).show()
//                null
//            })
//    }
//}
