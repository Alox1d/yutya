package ru.alox1d.yutya.presentation.ar

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.alox1d.yutya.data.Mask
import ru.alox1d.yutya.domain.repository.MaskRepository
import ru.alox1d.yutya.presentation.Event

class SearchCompanionViewModel : ViewModel() {
    private val _searchEvent = MutableLiveData<Event<Unit>>()
    val searchEvent: LiveData<Event<Unit>> = _searchEvent
    private val _setupEvent = MutableLiveData<Event<Unit>>()
    val setupEvent: LiveData<Event<Unit>> = _setupEvent
    private val _addMaskEvent = MutableLiveData<Event<Unit>>()
    val addMaskEvent: LiveData<Event<Unit>> = _addMaskEvent
    fun searchCompanion(){
        _searchEvent.value = Event(Unit)
    }

    fun setupMasks(){
        _setupEvent.value = Event(Unit)
    }
    fun addMask(){
        _addMaskEvent.value = Event(Unit)
    }
    // repository to get data.
    private val maskRepository: MaskRepository

    // LiveData: all masks
    var mMaskList: MutableLiveData<List<Mask>?>? = null
    private var mObservableMasks: LiveData<List<Mask>>? = null

    init {
        // LiveData
        if (mMaskList == null) {
            mMaskList = MutableLiveData()
        }
        maskRepository = MaskRepository.getInstance()
    }

    // LiveData => all masks
    fun getMaskByLiveData() {
        mObservableMasks = maskRepository.getAllByLiveData()
    }
    fun getObservableMasks(): LiveData<List<Mask>>? {
        if (mObservableMasks != null) {
            lgd("observableUsers: ${mObservableMasks.toString()}")
        } else {
            getMaskByLiveData()
        }
        return mObservableMasks
    }
    companion object {
        // logcat
        val tag = "MYLOG "+"MaskViewModel"
        fun lgi(s: String) { Log.i(tag, s)}
        fun lgd(s: String) { Log.d(tag, s)}
        fun lge(s: String) { Log.e(tag, s)}
    }
}