package ru.alox1d.yutya.presentation.maskoptions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.alox1d.yutya.R
import ru.alox1d.yutya.data.MaskDatabase.Companion.lgd
import ru.alox1d.yutya.databinding.FragmentMaskOptionsBinding
import ru.alox1d.yutya.presentation.ar.SearchCompanionViewModel

class MaskOptionsFragment : Fragment() {
    private val searchCompanionViewModel by activityViewModels<SearchCompanionViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentMaskOptionsBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.searchCompanionViewModel = searchCompanionViewModel
        setupMasks(binding.masksRv)




        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        if (item.itemId == R.id.action_chart) {
//            findNavController().navigate(R.id.action_result_chart_fragment)
//
//            return true
//        }

        return super.onOptionsItemSelected(item)
    }

    private fun setupMasks(recycler: RecyclerView) {
        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(activity) as RecyclerView.LayoutManager?
        val adapter = MasksAdapter(requireContext())
        recycler.adapter = adapter

        searchCompanionViewModel.getObservableMasks()!!.observe(
            viewLifecycleOwner,
             { masks ->
                if (masks != null) {
                    adapter.setMasks(masks)
                    lgd("user size: ${adapter.itemCount}")
                }
            })
        searchCompanionViewModel.addMaskEvent.observe(viewLifecycleOwner, { event ->
            event.getDataIfNotHandled()?.let {
                Toast.makeText(requireActivity(), getString(R.string.in_development), Toast.LENGTH_SHORT).show()
            }
        })
    }
}
