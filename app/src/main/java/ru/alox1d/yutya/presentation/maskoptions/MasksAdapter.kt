package ru.alox1d.yutya.presentation.maskoptions

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import ru.alox1d.yutya.R
import ru.alox1d.yutya.Utils.Companion.getBitmapFromAssets
import ru.alox1d.yutya.data.Mask

class MasksAdapter internal constructor(val context: Context) : RecyclerView.Adapter<MasksAdapter.MaskViewHolder>() {
    private val inflater: LayoutInflater =
        LayoutInflater.from(context)
    private var allMasks =
        emptyList<Mask?>() // Cached all masks
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MaskViewHolder {
        val inflater = LayoutInflater.from(parent.context)

//        val binding = ItemBinding.inflate(inflater, parent, false)
        val itemView = inflater.inflate(
            R.layout.item_mask, parent, false)
        return MaskViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MaskViewHolder, position: Int) {
        val current = allMasks[position]
        // String variable to hold specified image file name
//        val imageFileName = "masks/1_deer_face.png";
        // Set ImageView image from Assets folder
        val image = current?.path?.let { getBitmapFromAssets(context, it) }
        holder.maskImg.setImageBitmap(image);
        holder.btnDel.setOnClickListener {
            Toast.makeText(holder.btnDel.context, "Зачем удалять? Не надо", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int = allMasks.size
    internal fun setMasks(masks: List<Mask?>) {
        this.allMasks = masks
        notifyDataSetChanged()
    }

    class MaskViewHolder(itemView: View):
        RecyclerView.ViewHolder(itemView) {
        val maskImg: ImageView =
            itemView.findViewById(R.id.img_mask)
        val btnDel: Button =
            itemView.findViewById(R.id.btn_delete)

    }
}
