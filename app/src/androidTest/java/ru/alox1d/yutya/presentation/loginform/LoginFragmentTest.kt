package ru.alox1d.yutya.presentation.loginform

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import ru.alox1d.yutya.R
import ru.alox1d.yutya.presentation.ErrorMatcher
import ru.alox1d.yutya.presentation.MainActivity

@RunWith(AndroidJUnit4::class)
class LoginFragmentTest {
    @Test
    fun test_investFormIsVisible() {
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.fragment_invest_form_root)).check(matches(isDisplayed()))
    }

    @Test
    fun test_whenBalanceIsEmpty_showsError() {
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.login_form_sign_in)).perform(click())
        onView(withId(R.id.balance_layout)).check(matches(ErrorMatcher(R.string.login_form_error_empty_login)))
    }

    @Test
    fun test_whenBalanceIsZero_showsError() {
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.invest_form_balance)).perform(typeText("0"))
        onView(withId(R.id.login_form_sign_in)).perform(click())
        onView(withId(R.id.balance_layout)).check(matches(ErrorMatcher(R.string.login_form_error_zero_login)))
    }

    @Test
    fun test_whenPercentIsEmpty_showsError() {
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.login_form_sign_in)).perform(click())
        onView(withId(R.id.percent_layout)).check(matches(ErrorMatcher(R.string.login_form_error_empty_pass)))
    }

    @Test
    fun test_whenPercentIsZero_showsError() {
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.invest_form_percent)).perform(typeText("0"))
        onView(withId(R.id.login_form_sign_in)).perform(click())
        onView(withId(R.id.percent_layout)).check(matches(ErrorMatcher(R.string.invest_form_error_zero_percent)))
    }

    @Test
    fun test_whenIterationsIsEmpty_showsError() {
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.login_form_sign_in)).perform(click())
        onView(withId(R.id.iterations_layout)).check(matches(ErrorMatcher(R.string.invest_form_error_empty_iterations)))
    }

    @Test
    fun test_whenIterationsIsZero_showsError() {
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.invest_form_iterations)).perform(typeText("0"))
        onView(withId(R.id.login_form_sign_in)).perform(click())
        onView(withId(R.id.iterations_layout)).check(matches(ErrorMatcher(R.string.invest_form_error_zero_iterations)))
    }

    @Test
    fun test_whenAllFieldsAreValid_navigatesToResultsScreen() {
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.invest_form_balance)).perform(typeText(DEFAULT_BALANCE))
        onView(withId(R.id.invest_form_percent)).perform(typeText(DEFAULT_PERCENT))
        onView(withId(R.id.invest_form_iterations)).perform(typeText(DEFAULT_ITERATIONS))
        onView(withId(R.id.login_form_sign_in)).perform(click())
        onView(withId(R.id.fragment_invest_result_root)).check(matches(isDisplayed()))

        pressBack()

        onView(withId(R.id.fragment_invest_form_root)).check(matches(isDisplayed()))
    }

    companion object {
        private const val DEFAULT_BALANCE = "1000"
        private const val DEFAULT_PERCENT = "2.5"
        private const val DEFAULT_ITERATIONS = "8"
    }
}
