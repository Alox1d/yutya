
# Ютя - удивлю тя! 
# ВАЖНО: Запускать на Android с поддержкой OpenGL 3.1 и Android 7+

Приложение для масочных, (почти) анонимных видео-знакомств, которое удивит вас и собеседников (надеюсь, не вылетами)
Добавляйте СВОИ маски и регулируйте их под себя! (примечание мелким шрифтом: всего за 9.99$)
### Как это работает:
Пользователь логинится в приложении, и, если приобрёл подписку, может настроить коллекцию масок: добавить свои, изменить положение на лице и удалить. Если же нет, то нажимает кнопочку поиска и систему подбирает ему случайного человечка для общения.
С технической точки зрения используется библиотека Sceneform для распознавания лица и наложение маски, координаты положения и путь к которой хранятся в БД, для чего используется ORMка Room.

### Requirements
* Device with Android 7.0+ (API 24+)
* OpenGL 3.1 support

### How to install
1. Open project in Android Studio (tested on version 2020.3.1 Patch 3)
2. Press "Run App" (Shift+F10)

### Language and architecture

* Kotlin
* MVVM
* Clean architecture

### Libraries

* Sceneform SDK (AR)
* Room
* Data Binding
* LiveData
* ViewModel
* Navigation
* Single Activity
* AppCompat
* Android KTX
* Material
* Kotlin coroutines
* Dagger 2

### Video https://youtu.be/ib5cxF9NlGs

### Screenshots
<img src="https://i.ibb.co/V391bRW/2021-12-19-10-59-34.jpg" width="360" alt="Login" />
<img src="https://i.ibb.co/mCKK4yh/2021-12-19-10-59-38.jpg" width="360" alt="Home" />
<img src="https://i.ibb.co/VWFN7cG/2021-12-19-11-05-18.jpg" width="360" alt="Searching Companion" />
